#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
This app performs the synchonization with the system.

Python 2.5+
"""
from __future__ import with_statement
import os.path
import re
import filecmp
import getopt
import sys
import shutil
import itertools

def usage(app_name):
    """Usage message."""
    print """Syncpy  - synchronization tool by rlazo

This system was originally designed to work with data stored on a
versioning system, as git. If so _please_ update your copy before
doing anything else.

%s [option]
Options:
-h     Shows this message
-c     Diff between files stored and those in use.
-u     Overrides files in use using those stored. If some files 
       are missing they are created.""" % app_name

def diff_files(index):
    """Check for differences between the actual files and the
    reference ones. """
    issues = {'missing' : [], 'diff' : [], 'count' : 0}
    for value in index:
        path = os.path.expanduser("~/%s" % value)
        if not os.path.exists(path):
            issues['missing'].append(value)
        elif not filecmp.cmp(path, value, shallow=False):
            issues['diff'].append(value)
    issues['count'] = len(issues['missing']) + len(issues['diff'])
    return issues

def load_index(filename="index.txt"):
    """Load the index file. Lines starting with \# are considered
    comments.
    
    Arguments:
    - `filename`: index filename (path optional)
    """
    index = []
    with open(filename, 'r') as fd:
        ignore_regexp = re.compile(r"^ *(#.*| *)$")
        for line in fd:
            if ignore_regexp.match(line) is None:
                index.append(line.replace("\n", ""))
    return index

if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "cuh",[])
    except getopt.GetoptError, err:
        print str(err)
        usage(sys.argv[0])
        sys.exit(2)

    file_index = load_index()
    for o,a in opts:
        if o == "-c":
            res = diff_files(file_index)
            if res["count"] == 0:
                print "Everything is ok"
            else:
                for issue in res['missing']:
                    print "File %s missing." % issue
                for issue in res['diff']:
                    print "File %s is different." % issue
        elif o == "-u":
            ans = str(raw_input("Are you sure you want to override your files?[y/n] "))
            if ans.find("y") != -1:
                issues = diff_files(file_index)
                for dotfile in itertools.chain(issues['missing'], issues['diff']):
                    print "Copying %s ..." % dotfile
                    shutil.copy(dotfile, os.path.expanduser("~/%s" % dotfile))
                print "Done."
        elif o == "-s":
            print "Updating the repo..."
            issues = diff_files(file_index)
            for dotfile in issues['diff']:
                print "Copying %s ..." % dotfile
                shutil.copy(os.path.expanduser("~/%s" % dotfile), dotfile)
            print "Done."
        elif o == "-h":
            usage(sys.argv[0])
        else:
            usage(sys.argv[0])


