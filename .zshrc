export PATH=$HOME/bin:$HOME/lib/jdk1.6.0_01/bin:$HOME/local/bin/:$PATH
export PYTHONPATH=$HOME/lib/python:$HOME/local/lib64/python2.5/site-packages/:$PYTHONPATH

# Lines configured by zsh-newuser-install
HISTFILE=$HOME/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename "$HOME/.zshrc"

autoload -Uz compinit
compinit
# End of lines added by compinstall

# gentoo suggested 
autoload -U compinit promptinit
compinit
promptinit; prompt gentoo

setopt correct

# Keys
bindkey    "^[[3~"          delete-char
bindkey    "^[3;5~"         delete-char

# directories
setopt  autopushd pushdignoredups 

# Wordchars
WORDCHARS=${WORDCHARS//[.;\/]}

# alias
alias ls='ls --color=auto'
alias ll='ls -l'
alias rm='rm -i'
alias standby='xset dpms force standby'
alias aterm='aterm -sl 3000'
alias xpdff='xpdf -fullscreen'
alias mutt='mutt -y'
alias gits='git status'

# peluchera
cat $HOME/art/mensajitos/jaja

[ $TERM = "dumb" ] && unsetopt zle
