# python documentation
export PYTHONDOCS="/usr/share/doc/python-docs-2.4.4/html/"
export PYTHONPATH=$HOME/lib/python:$HOME/local/lib64/python2.5/site-packages/:$PYTHONPATH

# svn thu ssh
export SVN_SSH="ssh -l rlazo"

# var
export LANG="es_ES"
export LC_ALL="es_ES"
export IRCNICK="rlazo"

# qemu values
export QEMU_AUDIO_DAC_FIXED_FREQ=48000
export QEMU_AUDIO_ADC_FIXED_FREQ=48000 
export QEMU_ALSA_DAC_BUFFER_SIZE=4096 
export QEMU_AUDIO_DRV=alsa

export PATH=$HOME/bin:$HOME/lib/jdk1.6.0_01/bin:$PATH